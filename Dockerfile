FROM ubuntu:latest
ADD ts3.tgz /
CMD tar -zxvf ts3.tgz && rm ts3.tgz
CMD cd /server && ./ts3server_minimal_runscript.sh start || echo "Failed to start server. Exiting."
